﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{

    public partial class CalculatorForm : Form
    {
        public double number1;
        public double number2;
        public String operation;
        private readonly object TextBox1;

        Form2 form2 = new Form2();

        public CalculatorForm()
        {
            InitializeComponent();
        }

        private void powrótToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 form1=new Form1();
            //Close();
            form1.Show();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            textBox1.Text = string.Empty;
            number1 = 0;
            number2 = 0;
            operation = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text += button1.Text;
        }

        public void result()
        {
            number2 = System.Double.Parse(textBox1.Text);
            switch(operation)
            {
                case "+":
                    number1 = number1 + number2;
                    break;
                case "-":
                    number1 = number1 - number2;
                    break;
                case "*":
                    number1 = number1 * number2;
                    break;
                case "/":
                    number1 = number1 / number2;
                    break;
            }

            textBox1.Text = number1.ToString();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            number1 = System.Double.Parse(textBox1.Text);
            operation = "+";
            textBox1.Text = "";
        }

        private void button16_Click(object sender, EventArgs e)
        {
            result();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text += button2.Text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text += button3.Text;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Text += button4.Text;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text += button5.Text;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Text += button6.Text;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBox1.Text += button7.Text;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox1.Text += button8.Text;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            textBox1.Text += button9.Text;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            textBox1.Text += button10.Text;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            number1 = System.Double.Parse(textBox1.Text);
            operation = "-";
            textBox1.Text = "";
        }

        private void button11_Click(object sender, EventArgs e)
        {
            number1 = System.Double.Parse(textBox1.Text);
            operation = "*";
            textBox1.Text = "";
        }

        private void button12_Click(object sender, EventArgs e)
        {
            number1 = System.Double.Parse(textBox1.Text);
            operation = "/";
            textBox1.Text = "";
        }
    }
}
