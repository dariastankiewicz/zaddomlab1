﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hello!");
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonNewWindow_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
            //Close();
        }

        private void buttonBackgroundColor_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Bisque;
        }

        private void buttonMinimalize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button3Option_Click(object sender, EventArgs e)
        {

        }

        private void buttonCalculator_Click(object sender, EventArgs e)
        {
            CalculatorForm formCalculator = new CalculatorForm();
            formCalculator.Show();
        }
    }
}
