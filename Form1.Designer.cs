﻿namespace WindowsFormsApplication1
{
    public partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonNewWindow = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonAuthor = new System.Windows.Forms.Button();
            this.buttonBackgroundColor = new System.Windows.Forms.Button();
            this.buttonMinimalize = new System.Windows.Forms.Button();
            this.button3Option = new System.Windows.Forms.Button();
            this.buttonCalculator = new System.Windows.Forms.Button();
            this.labelForm1Message = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(237, 300);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(105, 41);
            this.buttonClose.TabIndex = 1;
            this.buttonClose.Text = "koniec pracy";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonNewWindow
            // 
            this.buttonNewWindow.Location = new System.Drawing.Point(12, 60);
            this.buttonNewWindow.Name = "buttonNewWindow";
            this.buttonNewWindow.Size = new System.Drawing.Size(152, 42);
            this.buttonNewWindow.TabIndex = 6;
            this.buttonNewWindow.Text = "Okno logowania";
            this.buttonNewWindow.UseVisualStyleBackColor = true;
            this.buttonNewWindow.Click += new System.EventHandler(this.buttonNewWindow_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 34);
            this.label1.TabIndex = 7;
            this.label1.Text = "To można zrobić \r\nz aktualnym okienkiem:";
            // 
            // buttonAuthor
            // 
            this.buttonAuthor.Location = new System.Drawing.Point(264, 60);
            this.buttonAuthor.Name = "buttonAuthor";
            this.buttonAuthor.Size = new System.Drawing.Size(105, 32);
            this.buttonAuthor.TabIndex = 8;
            this.buttonAuthor.Text = "Autor";
            this.buttonAuthor.UseVisualStyleBackColor = true;
            // 
            // buttonBackgroundColor
            // 
            this.buttonBackgroundColor.Location = new System.Drawing.Point(13, 167);
            this.buttonBackgroundColor.Name = "buttonBackgroundColor";
            this.buttonBackgroundColor.Size = new System.Drawing.Size(119, 32);
            this.buttonBackgroundColor.TabIndex = 9;
            this.buttonBackgroundColor.Text = "zmiana tła";
            this.buttonBackgroundColor.UseVisualStyleBackColor = true;
            this.buttonBackgroundColor.Click += new System.EventHandler(this.buttonBackgroundColor_Click);
            // 
            // buttonMinimalize
            // 
            this.buttonMinimalize.Location = new System.Drawing.Point(13, 205);
            this.buttonMinimalize.Name = "buttonMinimalize";
            this.buttonMinimalize.Size = new System.Drawing.Size(119, 32);
            this.buttonMinimalize.TabIndex = 10;
            this.buttonMinimalize.Text = "minimalizuj";
            this.buttonMinimalize.UseVisualStyleBackColor = true;
            this.buttonMinimalize.Click += new System.EventHandler(this.buttonMinimalize_Click);
            // 
            // button3Option
            // 
            this.button3Option.Location = new System.Drawing.Point(15, 243);
            this.button3Option.Name = "button3Option";
            this.button3Option.Size = new System.Drawing.Size(117, 35);
            this.button3Option.TabIndex = 11;
            this.button3Option.Text = "opcja zagadka";
            this.button3Option.UseVisualStyleBackColor = true;
            this.button3Option.Click += new System.EventHandler(this.button3Option_Click);
            // 
            // buttonCalculator
            // 
            this.buttonCalculator.Location = new System.Drawing.Point(15, 300);
            this.buttonCalculator.Name = "buttonCalculator";
            this.buttonCalculator.Size = new System.Drawing.Size(107, 41);
            this.buttonCalculator.TabIndex = 12;
            this.buttonCalculator.Text = "kalkulator";
            this.buttonCalculator.UseVisualStyleBackColor = true;
            this.buttonCalculator.Click += new System.EventHandler(this.buttonCalculator_Click);
            // 
            // labelForm1Message
            // 
            this.labelForm1Message.AutoSize = true;
            this.labelForm1Message.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelForm1Message.Location = new System.Drawing.Point(28, 19);
            this.labelForm1Message.Name = "labelForm1Message";
            this.labelForm1Message.Size = new System.Drawing.Size(314, 34);
            this.labelForm1Message.TabIndex = 13;
            this.labelForm1Message.Text = "Program, który łączy wszystkie zadania domowe \r\nz pierwszego laboratorium.";
            this.labelForm1Message.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 353);
            this.Controls.Add(this.labelForm1Message);
            this.Controls.Add(this.buttonCalculator);
            this.Controls.Add(this.button3Option);
            this.Controls.Add(this.buttonMinimalize);
            this.Controls.Add(this.buttonBackgroundColor);
            this.Controls.Add(this.buttonAuthor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonNewWindow);
            this.Controls.Add(this.buttonClose);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonNewWindow;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonAuthor;
        private System.Windows.Forms.Button buttonBackgroundColor;
        private System.Windows.Forms.Button buttonMinimalize;
        private System.Windows.Forms.Button button3Option;
        private System.Windows.Forms.Button buttonCalculator;
        private System.Windows.Forms.Label labelForm1Message;
    }
}

